#encoding:utf-8-#
#(c)copyright Jan Straka 2019#
import sys

def crypt(text, key):
    table = "qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM#<>_!`+="
    i = 0
    for_return = ""
    while i < len(text):
        if list(text)[i] != " ":
            cnt = table.find(list(text)[i]) + key
            if cnt > len(table):
                for_return = for_return + list(table)[table.find(list(text)[i]) + key - len(table)]
            else:
                 for_return = for_return + list(table)[table.find(list(text)[i]) + key]
        else:
            for_return = for_return + "-"
        i += 1
    return for_return

def decrypt(text, key):
    table = "qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDFGHJKLYXCVBNM#<>_!`+="
    i = 0
    for_return = ""
    while i < len(text):
        if list(text)[i] != "-":
            cnt = table.find(list(text)[i]) + key
            if cnt < len(table):
                for_return = for_return + list(table)[table.find(list(text)[i]) - key + len(table)]
            else:
                 for_return = for_return + list(table)[table.find(list(text)[i]) - key]
        else:
            space = " "
            if space != " ":
                space = " "
            for_return = for_return + space
        i += 1
    return for_return
